	import React, { Component } from 'react'


	import {  Form, Row, Col } from 'react-bootstrap';

	const openBraces  = "{";
	const closeBraces = "}";

	class FormJson extends Component {

		constructor(props) {
			super(props);
			this.state = {
				ad_name: '',
				dt_start: '', 
				dt_end: '', 
				url_image: '', 
				url_destination: '', 
				priority: '',
				rules: '',
				pages: [
					{
						id:1,
						name: 'all'
					},
					{
						id:2,
						name: 'assinaturas'
					},
					{
						id:3,
						name: 'colunas'
					},
					{
						id:4,
						name: 'coluna do especialista'
					},
					{
						id:5,
						name: 'contato'
					},
					{
						id:6,
						name: 'depoimentos'
					},
					{
						id:7,
						name: 'doenças'
					},
					{
						id:8,
						name: 'especialista'
					},
					{
						id:9,
						name: 'notícias'
					},
					{
						id:10,
						name: 'home'
					},
					{
						id:11,
						name: 'interna artigo'
					},
					{
						id:12,
						name: 'interna assinatura'
					},
					{
						id:13,
						name: 'interna doenças'
					},
					{
						id:14,
						name: 'interna especialista'
					},
					{
						id:15,
						name: 'interna da notícia'
					},
					{
						id:16,
						name: 'jolivi é confiável?'
					},
					{
						id:17,
						name: 'podcasts'
					},
					{
						id:18,
						name: 'quem somos'
					},
					{
						id:19,
						name: 'vídeos'
					},
				],
				sections: [	
				{
					id: 1,
					name: 'header'
				},
				{
					id: 2,
					name: 'footer'
				},
				{
					id: 3,
					name: 'sidebar'
				},
				{
					id: 4,
					name: 'conteúdo do post'
				},
				{
					id: 5,
					name: 'as mais lidas 1'
				},
				{
					id: 6,
					name: 'as mais lidas 2'
				},
				{
					id: 7,
					name: 'últimas notícias 1'
				},
				{
					id: 8,
					name: 'últimas notícias 2'
				},
				],
				devices: [	
				{
					id: 1,
					name: 'mobile'
				},
				{
					id: 2,
					name: 'desktop'
				},
				{
					id: 3,
					name: 'all'
				},
				],
				selectedDevice: [],
				selectedSection: [],
				selectedPage: [],
			};

		}

		handleInputChange = (event) => {
		    const target = event.target;
		    const value = target.type === 'checkbox' ? target.checked : target.value;
		    const name = target.name;

		    this.setState({
		      [name]: value
		    });
		  }

		onChange = (id) => {
			let selected = this.state.selectedDevice
			let find = selected.indexOf(id)

			if(find > -1) {
				selected.splice(find, 1)
			} else {
				selected.push(id)
			}

			this.setState({ selected })
		}

		onChangeSec = (id) => {
			let selected = this.state.selectedSection
			let find = selected.indexOf(id)

			if(find > -1) {
				selected.splice(find, 1)
			} else {
				selected.push(id)
			}
			this.setState({ selected })
		}

		onChangePag = (id) => {
			let selected = this.state.selectedPage
			let find = selected.indexOf(id)

			if(find > -1) {
				selected.splice(find, 1)
			} else {
				selected.push(id)
			}

			this.setState({ selected })
		}

		render() {
			return (
				<div>
					<div className="column col-left">

						<Form.Group>
							<Row>
								<Col sm={12}>
									<Form.Label>Título</Form.Label>
									<Form.Control
										size="sm"
										type="text"
										name="ad_name"
										value={this.state.value}
										onChange={this.handleInputChange}
									/>
								</Col>
							</Row>
					</Form.Group>

					<Form.Group>
						<Row>
					
							<Col sm={4}>
								<Form.Label>Início</Form.Label>
								<Form.Control
									size="sm"
									type="text"
									name="dt_start"
									value={this.state.value}
									onChange={this.handleInputChange}
								/>
							</Col>

							<Col sm={4}>
								<Form.Label>Fim</Form.Label>
									<Form.Control
										size="sm"
										name="dt_end"
										type="text"
										value={this.state.value}
										onChange={this.handleInputChange}
									/>
							</Col>

							<Col sm={4}>
								<Form.Label>Prioridade</Form.Label>
							
								<Form.Select
									size="sm"
									name="priority"
									value={this.state.value}
									onChange={this.handleInputChange}>
										<option>Selecione</option>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
								</Form.Select>
							</Col>
						</Row>
					</Form.Group>

					<Form.Group>
						<Row>
							<Col sm={12}>			
								<Form.Label>Url da imagem</Form.Label>
								<Form.Control
									size="sm"
									name="url_image"
									type="text"
									value={this.state.value}
									onChange={this.handleInputChange}
								/>
							</Col>
						</Row>
					</Form.Group>

					<Form.Group>
						<Row>
							<Col sm={12}>
								<Form.Label>url de destino</Form.Label>
								<Form.Control
									size="sm"
									name="url_destination"
									type="text"
									value={this.state.value}
									onChange={this.handleInputChange}
								/>
							</Col>
						</Row>
					</Form.Group>

					<Form.Group>
						<Row>
							<Col sm={6}>
								<Form.Label>Páginas:</Form.Label>
								{
									this.state.pages.map(item => {
										return (
											<div key={ item.id }>
											<Form.Label>
											<Form.Check
											inline
											name="pages"
											type="checkbox"
											onChange={ () => this.onChangePag(item.name) }
											selected={ this.state.selectedPage.includes(item.name) }
											/>
											<span>{ item.name }</span>
											</Form.Label>
											</div>
										)
									})
								}
							</Col>

							<Col sm={6}>
								<Form.Label>Sections:</Form.Label>
								{
									this.state.sections.map(item => {
										return (
											<div key={ item.id }>
											<Form.Label>
											<Form.Check
											inline
											name="section"
											type="checkbox"
											onChange={ () => this.onChangeSec(item.name) }
											selected={ this.state.selectedSection.includes(item.name) }
											/>
											<span>{ item.name }</span>
											</Form.Label>
											</div>
										)
									})
								}
							</Col>
						</Row>
					</Form.Group>
					
					<Form.Group>
						<Row>
							<Col sm={12}>
								<Form.Label>Devices:</Form.Label>
								{
									this.state.devices.map(item => {
										return (
											<div key={ item.id }>
											<Form.Label>
											<Form.Check
											inline
											name="devices"
											type="checkbox"
											onChange={ () => this.onChange(item.name) }
											selected={ this.state.selectedDevice.includes(item.name) }
											/>
											<span>{ item.name }</span>
											</Form.Label>
											</div>
										)
									})
								}
							</Col>
					</Row>
					</Form.Group>
			
					<Form.Group>
						<Row>
							<Col sm={12}>
								<Form.Label>Rules</Form.Label>
									<Form.Control
										size="sm"
										name="rules"
										type="text"
										value={this.state.value}
										onChange={this.handleInputChange}
									/>
							</Col>
						</Row>
					</Form.Group>
				</div>
				<div className="column col-right">
					<div className="result">
						<pre className="braces">{openBraces}</pre>
						<pre>"ad_name": {JSON.stringify(this.state.ad_name)},</pre>
						<pre>"dt_start": {JSON.stringify(this.state.dt_start)},</pre> 
						<pre>"dt_end": {JSON.stringify(this.state.dt_end)},</pre> 
						<pre>"url_image": {JSON.stringify(this.state.url_image)},</pre> 
						<pre>"url_destination": {JSON.stringify(this.state.url_destination)},</pre> 
						<pre>"priority": {this.state.priority},</pre>
						<pre>"sections":{JSON.stringify(this.state.selectedSection) },</pre>
						<pre>"pages":{JSON.stringify(this.state.selectedPage) },</pre>
						<pre>"devices":{JSON.stringify(this.state.selectedDevice) },</pre>
						<pre>"rules": {JSON.stringify(this.state.rules) } </pre>
						<pre className="braces">{closeBraces}</pre>
					</div>
				</div>
			</div>
			);

		}
	}

	export default FormJson;






			
			

