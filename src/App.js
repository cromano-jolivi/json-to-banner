import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import FormJson from './components/Form.jsx'

function App() {
  return (
    <div className="App">
      <FormJson />
    </div>
  );
}

export default App;
